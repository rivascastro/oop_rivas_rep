import java.util.Objects;

public class Datum {

	private int tag;
	private int monate;
	private int jahr;

	public Datum(int tag, int monate, int jahr) {
		setTag(tag);
		setMonate(monate);
		setJahr(jahr);
	}

	public Datum() {
		setTag(1);
		setMonate(1);
		setJahr(1970);
	}

	public void setTag(int tag) {
		this.tag = tag;
	}

	public void setMonate(int monate) {
		this.monate = monate;
	}

	public void setJahr(int jahr) {
		this.jahr = jahr;
	}

	public int getTag() {
		return tag;
	}

	public int getMonate() {
		return monate;
	}

	public int getJahr() {
		return jahr;
	}

	@Override
	public String toString() {
		return "Datum [ " +tag +"."+ monate +"."+ jahr + " ]";
	}

	@Override
	public boolean equals(Object obj) {
		if ( obj instanceof Datum) {
			Datum other = (Datum) obj;
			return this.tag==other.getTag() && this.monate==other.getMonate() && this.jahr==other.getJahr();
		}
		else
			return false;
	}
		
}
