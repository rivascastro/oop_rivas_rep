import java.util.Scanner;

public class Grosshaendler01 {

	public static void main(String[] args) {

		Scanner miEscanner = new Scanner(System.in);
		System.out.println("Preis von PC-Maus?");
		double preis = miEscanner.nextDouble();
		System.out.println("Wie viel davon?");
		int quantitaet = miEscanner.nextInt();

		double endWert = preis * quantitaet * 1.19;
		int transporte = 10;
		double endsumme = endWert + transporte;

		if (quantitaet >= 10) {
			System.out.println("Gl�ckwusch, Lieferung Gratis!");
			System.out.printf("Brutto-Preis zu zahlen: " + "%.2f%n", endWert);
		} else {
			System.out.println("Achtung: 10Euro Transport-kosten kommen dazu!");
			System.out.printf("Gesamtpreis zu zahlen: " + "%.2f%n", endsumme);
		}

	}

}