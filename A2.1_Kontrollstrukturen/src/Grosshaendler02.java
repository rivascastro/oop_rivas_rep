
import java.util.Scanner;

public class Grosshaendler02 {

	public static void main(String[] args) {

		Scanner miEscanneador = new Scanner(System.in);
		System.out.println("Preis von Ware?");
		double bestellpreis = miEscanneador.nextDouble();

		if (bestellpreis <= 100) {
			System.out.printf("Preis inklusiv 10%% Rabatt und MwSt  = %.2f Euro", 0.9 * bestellpreis);
		} else if (bestellpreis > 100 && bestellpreis <= 500) {
			System.out.printf("Preis inklusiv 15%% Rabatt und MwSt  = %.2f Euro", 0.85 * bestellpreis);
		} else {
			System.out.printf("Preis inklusiv 20%% Rabatt und MwSt  = %.2f Euro", 0.80 * bestellpreis);
		}
	}
}
